package shared

/**
Source: https://stackoverflow.com/a/31832326
*/

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/hmac"
	crand "crypto/rand"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"io"
	"math/rand"
	"time"
	"unsafe"
)

var (
	randomSrc = rand.NewSource(time.Now().UnixNano())
	err       error
	outputRaw []byte
	gcm       cipher.AEAD
	block     cipher.Block
)

func GenerateRandomString(n int, seeder Seeder) string {
	seed := ""
	if seeder == LETTERONLY {
		seed = LetterConst
	} else if seeder == ALPHANUM {
		seed = AlphaNumConst
	} else if seeder == CLEANEDLETTER {
		seed = CleanedConst
	}
	b := make([]byte, n)
	// A randomSrc.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, randomSrc.Int63(), LetterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = randomSrc.Int63(), LetterIdxMax
		}
		if idx := int(cache & LetterIdxMask); idx < len(seed) {
			b[i] = seed[idx]
			i--
		}
		cache >>= LetterIdxBits
		remain--
	}

	return *(*string)(unsafe.Pointer(&b))
}

// GenerateSignatureSHA1HMAC
// One way fast hash
// Params: String input
// Params: String key
// Returns: Array byte
func GenerateSignatureSHA1HMAC(input string, key string) []byte {
	byteKey := []byte(key)
	hash := hmac.New(sha1.New, byteKey)
	hash.Write([]byte(input))
	return hash.Sum(nil)
}

func EncodeBase64(input []byte) string {
	return base64.StdEncoding.EncodeToString(input)
}

func DecodeBase64(input string) ([]byte, error) {
	return base64.StdEncoding.DecodeString(input)
}

func EncodeBase64FromString(inputString string) string {
	input := []byte(inputString)
	return base64.StdEncoding.EncodeToString(input)
}

func DecodeBase64ToString(input string) (string, error) {
	outputRaw, err = base64.StdEncoding.DecodeString(input)
	if err != nil {
		return "", err
	}
	return string(outputRaw), nil
}

/*
  - =========================== Simple Crypt ===========================
    Need to assign your own nonce
*/
func cipherKey(nonce, salt string) []byte {
	return append(append(append([]byte(nonce)[:], ENC_KEY_32[:]...), []byte(salt)[:]...), ENC_KEY_16[:]...)
}

func SimpleEncrypt(data, nonce, salt string) (string, error) {
	cipherkey := cipherKey(nonce, salt)
	block, err = aes.NewCipher(cipherkey)
	if err != nil {
		return "", err
	}

	rawData := []byte(data)
	cipherEnc := cipher.NewCFBEncrypter(block, ENC_KEY_16_FREE)
	cipherTxt := make([]byte, len(rawData))
	cipherEnc.XORKeyStream(cipherTxt, rawData)
	return EncodeBase64(cipherTxt), nil
}

func SimpleDecrypt(data, nonce, salt string) (string, error) {
	cipherkey := cipherKey(nonce, salt)
	block, err = aes.NewCipher(cipherkey)
	if err != nil {
		return "", err
	}

	outputRaw, err = DecodeBase64(data)
	if err != nil {
		return "", err
	}

	cipherDec := cipher.NewCFBDecrypter(block, ENC_KEY_16_FREE)
	cipherTxt := make([]byte, len(outputRaw))
	cipherDec.XORKeyStream(cipherTxt, outputRaw)
	return string(cipherTxt), nil
}

/*
  - =========================== GCM Crypt ===========================
    Nonce are auto assigned
*/
func generateLockKey() error {
	var cryptor cipher.Block
	cryptor, err = aes.NewCipher(ENC_KEY_32[:])
	if err != nil {
		err = fmt.Errorf("failed to open cryptor: %v", err)
		return err
	}

	gcm, err = cipher.NewGCM(cryptor)
	if err != nil {
		err = fmt.Errorf("failed to execute cryptor: %v", err)
	}

	return err
}

func GCMEncrypt(data string) (string, error) {
	if err = generateLockKey(); err != nil {
		return "", err
	}

	dt := []byte(data)
	nonce := make([]byte, gcm.NonceSize())

	if _, err = io.ReadFull(crand.Reader, nonce); err != nil {
		err = fmt.Errorf("failed to generate nonce: %v", err)
		return "", err
	}
	return base64.StdEncoding.EncodeToString(gcm.Seal(nonce, nonce, dt, nil)), nil
}

func GCMDecrypt(data string) (string, error) {
	if err = generateLockKey(); err != nil {
		return "", err
	}

	var dt []byte
	var result []byte

	dt, err = base64.StdEncoding.DecodeString(data)
	if err != nil {
		err = fmt.Errorf("failed to decode base64: %v", err)
		return "", err
	}
	nonceSize := gcm.NonceSize()
	if len(dt) < nonceSize {
		err = fmt.Errorf("data length mismatch")
		return "", err
	}
	nonce, cipherText := dt[:nonceSize], dt[nonceSize:]
	result, err = gcm.Open(nil, nonce, cipherText, nil)
	if err != nil {
		err = fmt.Errorf("failed to decrypt data: %v", err)
		return "", err
	}
	return string(result), nil
}
