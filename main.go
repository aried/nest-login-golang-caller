package main

import (
	"NestJS-Microservice-tester/shared"
	"bytes"
	"encoding/json"
	"fmt"
	"net"
	"strconv"
	"strings"
)

func main() {
	connection, err := net.Dial("tcp", "localhost:3001")
	if err != nil {
		panic("cannot connect to tcp")
	}

	randomId := shared.GenerateRandomString(16, shared.ALPHANUM)

	data := map[string]interface{}{
		"data": map[string]interface{}{
			"username": "aried",
			"password": "wakawaku",
		},
		"pattern": map[string]string{
			"internalCmd": "login",
		},
		"id": randomId,
	}
	send, _ := json.Marshal(data)
	sendString := string(send)
	sendDataLength := len(sendString)
	sendData := fmt.Sprintf("%v#%s", sendDataLength, sendString)

	_, err = connection.Write([]byte(sendData))
	if err != nil {
		panic(err)
	}

	receiveData := make([]byte, 1024)
	connection.Read(receiveData)
	receiveData = bytes.Trim(receiveData, "\x00")
	receivedArray := strings.Split(string(receiveData), "#")

	if receivedLength, _ := strconv.Atoi(receivedArray[0]); len(receivedArray[1]) == receivedLength {
		var received TCPResponse
		dta := strings.Trim(receivedArray[1], "")
		fmt.Println("DTA", dta)
		parsedData := []byte(dta)

		json.Unmarshal(parsedData, &received)

		fmt.Println("Got it!", received.ID)

		fmt.Println("ID", received.ID)
		if randomId == received.ID {
			fmt.Println("Check OK")
			fmt.Println(received.Response.Message)
			fmt.Println(received.Response.Token)
			fmt.Println(received.Response.Valid)
		} else {
			panic("failed check")
		}

	} else {
		fmt.Println("FAILED:", receiveData[0], receiveData[1])
	}
	connection.Close()
}

type TCPResponse struct {
	ID         string
	IsDisposed bool
	Response   messageResponse
}

type messageResponse struct {
	Message string
	Token   string
	Valid   bool
}
