package shared

// array is not immutable, so only thing we can do is...
var (
	ENC_KEY_32 = [32]byte{
		20, 21, 59, 80, 57, 27, 19, 66,
		46, 69, 47, 67, 51, 39, 29, 17,
		16, 34, 37, 62, 11, 10, 78, 40,
		70, 13, 24, 35, 73, 77, 71, 32,
	}
	ENC_KEY_16 = [16]byte{
		38, 12, 59, 13, 69, 76, 19, 75,
		60, 46, 58, 43, 41, 47, 17, 45,
	}
	ENC_KEY_8 = [8]byte{
		27, 13, 74, 44, 58, 31, 24, 41,
	}
	ENC_KEY_16_FREE = []byte{
		35, 46, 57, 24, 85, 35, 24, 74,
		87, 35, 88, 98, 66, 32, 14, 05,
	}
)

type VersionSeverity int32

const (
	VS_NONE VersionSeverity = iota
	VS_LOW
	VS_MID
	VS_HIGH
	VS_MANDATORY
)

type Seeder int8

const (
	LetterConst   = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	AlphaNumConst = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	CleanedConst  = "abcdefghjkmnpqrtuvwxyz2346789ABCDEFGHJKLMNPQRTUVWXYZ"
	LetterIdxBits = 6                    // 6 bits to represent a letter index
	LetterIdxMask = 1<<LetterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	LetterIdxMax  = 63 / LetterIdxBits   // # of letter indices fitting in 63 bits

	LETTERONLY Seeder = iota + 4
	ALPHANUM
	CLEANEDLETTER
)
